#!/usr/bin/env python
import rospy
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Pose2D
import numpy as np
from enum import Enum
from itertools import product
from collections import OrderedDict


class State(Enum):
    TOO_CLOSE = 0
    TOO_FAR = 1
    GOOD = 2


class Action(Enum):
    LEFT = 0
    RIGHT = 1
    FWD = 2


pub = rospy.Publisher("triton_lidar/vel_cmd", Pose2D, queue_size=2)
current_state = (State.TOO_FAR, State.TOO_FAR, State.TOO_FAR)
prev_state = current_state
current_action = Action.FWD
prev_action = current_action

LIN_SPD = 0.3
TRN_SPD = 0.78
first_wall = False

action_list = [Action.LEFT, Action.RIGHT, Action.RIGHT,
        Action.FWD, Action.RIGHT, Action.RIGHT,
        Action.FWD, Action.RIGHT, Action.RIGHT,
        Action.LEFT, Action.LEFT, Action.LEFT,
        Action.LEFT, Action.FWD, Action.RIGHT,
        Action.LEFT, Action.FWD, Action.FWD,
        Action.LEFT, Action.LEFT, Action.LEFT,
        Action.LEFT, Action.RIGHT, Action.FWD,
        Action.LEFT, Action.RIGHT, Action.FWD
        ]


state_comb = [State.TOO_CLOSE, State.TOO_FAR, State.GOOD]
states = product(state_comb, repeat=3)
q_table = OrderedDict(zip(states, action_list))

def reset_q_table(q_table):
    # reset all entries in q_table to be forward
    for key in q_table:
        q_table[key] = Action(2)
    return q_table


def getState(distance):
    if distance < 0.5:
        return State.TOO_CLOSE
    elif 0.5 <= distance <= 0.8:
        return State.GOOD
    elif distance > 0.8:
        return State.TOO_FAR


def getLaserScan():
    rospy.Subscriber("scan", LaserScan, updateAll)
    rospy.spin()


def updateState(data):
    global prev_state
    global current_state
    zones = np.zeros((3,))
    fwd = []
    left = []
    right = []
    scan_len = len(data.ranges)
    
    deg = np.pi / 180
    for i in range(scan_len):
        val = data.ranges[i]
        angle = i * deg
        if val > data.range_max:
            val = data.range_max
        elif val < data.range_min:
            val = data.range_min
        
        # debug only
        #rospy.logerr("value of angle is %s", angle)
        
        if (0 <= angle <= (20 * np.pi / 180) 
            or (340 * np.pi / 180) <= angle <= 6.28):
            right.append(val)
        elif (75 *np.pi / 180)  <= angle <= (105 * np.pi / 180):
            fwd.append(val)
        elif (160 * np.pi / 180) <= angle <= (200 * np.pi / 180):
            left.append(val)

    zones[0] = min(left)
    zones[1] = min(fwd)
    zones[2] = min(right)

    prev_state = current_state
    current_state = (getState(zones[0]), getState(zones[1]), 
            getState(zones[2]))


def debug_logging():
    rospy.logerr("Current State is: %s", current_state)
    rospy.logerr("Current Action is: %s", current_action)
    rospy.logerr("q_table is: %s", q_table)

def updateAction():
    global prev_action, current_action, current_state, first_wall
    prev_action = current_action
    if not first_wall:
        if (current_state[0] == State.TOO_FAR
            and current_state[1] == State.TOO_FAR
            and current_state[2] == State.TOO_FAR):
            first_wall = True
    else:
        current_action = Action(q_table[current_state])


def publishAction():
    global pub
    mvmt = Pose2D()
    if current_action.value == 0:
        mvmt.theta = TRN_SPD
        #mvmt.x = -0.25 * LIN_SPD
    elif current_action.value == 1:
        mvmt.theta = -1 * TRN_SPD
        #mvmt.x = 0.25 * LIN_SPD
    elif current_action.value == 2:
        mvmt.theta = 0
        #mvmt.x = 0
    mvmt.y = LIN_SPD
    pub.publish(mvmt)


def updateAll(data):
    updateState(data)
    updateAction()
    #debug_logging()
    publishAction()


if __name__ == "__main__":
    try:
        rospy.init_node("wall_follower", anonymous=True, log_level=rospy.ERROR)
        getLaserScan()
    
    except rospy.ROSInterruptionException:
        pass

