#!/usr/bin/env python

import rospy
from std_srvs.srv import Empty
from gazebo_msgs.srv import GetModelState
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Pose2D
import numpy as np
from enum import Enum
from itertools import product
from collections import OrderedDict


class State(Enum):
    TOO_CLOSE = 0
    TOO_FAR = 1
    GOOD = 2


class Action(Enum):
    LEFT = 0
    RIGHT = 1
    FWD = 2


pub = rospy.Publisher("triton_lidar/vel_cmd", Pose2D, queue_size=2)
current_state = (State.TOO_FAR, State.TOO_FAR, State.TOO_FAR)
prev_state = current_state
current_action = Action.FWD
prev_action = current_action

LIN_SPD = 0.3
TRN_SPD = 1.6


state_comb = [State.TOO_CLOSE, State.TOO_FAR, State.GOOD]
states = product(state_comb, repeat=3)
state_list = list(states)
state_size = len(state_list)
actions = np.zeros((state_size), dtype="f,f,f")
learning_table = OrderedDict(zip(state_list, actions))

episode = 0
termination_steps = 10000
max_steps = 1000
learning_rate = 0.3
discount = 0.9
steps = 0
episode_reward = 0
wall_steps = 0
trapped_steps = 0
prev_pos = [0, 0, 0]
pos_tol = 5e-3


def reset_learning_table(learning_table):
    print("resetting learning table")
    for key in learning_table:
        learning_table[key] = (0,0,0)
    return learning_table


def updateEpisode():
    global steps, wall_steps, trapped_steps
    global episode, episode_reward
    global learning_table
    global prev_pos
    rospy.wait_for_service("/gazebo/reset_simulation")
    reset_sim = rospy.ServiceProxy("/gazebo/reset_simulation", Empty)
    reset_sim()
    print("Episode {} done in {} steps".format(episode, steps))
    print("Episode had score {}".format(episode_reward))
    
    rospy.wait_for_service("/gazebo/get_model_state")
    robot_pos_srv = rospy.ServiceProxy("/gazebo/get_model_state",
            GetModelState)
    robot = robot_pos_srv("triton_lidar", "world")
    prev_pos = [robot.pose.position.x, robot.pose.position.y,
            robot.pose.position.z]
    episode += 1
    episode_reward = 0
    steps = 0
    wall_steps = 0
    trapped_steps = 0


def reward_func(state):
    global wall_steps
    val = 0
    if state[2] == State.GOOD:
        val += 0.8
            if state[1] == State.TOO_FAR:
                val += 0.2
    elif (state[1] == State.TOO_CLOSE and (state[0] == State.TOO_CLOSE 
        or state[0] == State.GOOD)):
        val -= 1
    
    #if (state[2] == State.TOO_CLOSE or state[2] == State.TOO_FAR
    #        or state[1] == State.TOO_CLOSE or state[0] == State.GOOD
    #        or state[0] == State.TOO_CLOSE):
    #    val -= 1
    #    wall_steps = 0
    #elif state[2] == State.GOOD:
    #    wall_steps += 1
    #    val += 0.8
    #    if (state[1] == State.TOO_FAR):
    #        val += 0.1
    #    if wall_steps % 10 == 0:
    #        val += 1
    elif (state[0] == State.TOO_FAR and state[1] == State.TOO_FAR
            and state[2] == State.TOO_FAR):
        val -= 0.5
    elif state[2] == State.TOO_CLOSE:
        val -= 0.3
    return val


def updateAll(data):
    global steps, episode_reward
    steps += 1
    updateState(data)
    updateAction()
    # debug_logging()
    publishAction()
    episode_reward += reward_func(current_state)
    updateTable() 
    if steps % 30 == 0:
        print("Episode reward so far is {}".format(episode_reward)) 
    
    if finishEpisode(data):
        updateEpisode()


def finishEpisode(data):
    global trapped_steps, prev_pos
    global learning_rate
    if steps >= termination_steps or wall_steps == max_steps:
        print("steps at {}, followed wall for {}".format(steps, wall_steps))
        return True
    else:
        rospy.wait_for_service("/gazebo/get_model_state")
        robot_pos_srv = rospy.ServiceProxy("/gazebo/get_model_state",
                GetModelState)
        robot = robot_pos_srv("triton_lidar", "world")
        robot_pos = [robot.pose.position.x, robot.pose.position.y,
                robot.pose.position.z]
        if robot_pos[2] > 0.5:
            print("robot is climbing the wall, resetting sim")
            return True
        elif steps > 2:
            pos_difs = [abs(prv-cur) for prv, cur in zip(prev_pos, robot_pos)]
            prev_pos = robot_pos
            if (pos_difs[0] < pos_tol and pos_difs[1] < pos_tol
                    and pos_difs[2] < pos_tol):
                learning_rate = 0.8
                trapped_steps += 1
                if trapped_steps >= 3:
                    print("position stayed same too long, ending episode")
                    return True
            else:
                trapped_steps = 0
        else:
            return False


def updateTable():
    global learning_table
    prev_action_idx = prev_action.value
    state_vals = list(learning_table[prev_state])
    q_val = (learning_table[prev_state][prev_action_idx] + 
            learning_rate * (reward_func(prev_state) + discount * 
                     max(learning_table[current_state]) - 
                     learning_table[prev_state][prev_action_idx]))
    state_vals[prev_action_idx] = q_val
    learning_table[prev_state] = state_vals


def getState(distance):
    if distance < 0.5:
        return State.TOO_CLOSE
    elif 0.5 <= distance <= 1.0:
        return State.GOOD
    elif distance > 1.0:
        return State.TOO_FAR


def getLaserScan():
    rospy.Subscriber("scan", LaserScan, updateAll)
    rospy.spin()


def updateState(data):
    global prev_state
    global current_state
    zones = np.zeros((3,))
    fwd = []
    left = []
    right = []
    scan_len = len(data.ranges)
    angle_size = 0.3
    deg = np.pi / 180
    for i in range(scan_len):
        val = data.ranges[i]
        angle = i * deg
        if val > data.range_max:
            val = data.range_max
        elif val < data.range_min:
            val = data.range_min
        
        
        #if (0 <= angle <= (20 * np.pi / 180) 
        #    or (340 * np.pi / 180) <= angle <= 6.28):
        #    right.append(val)
        #elif (70 *np.pi / 180)  <= angle <= (110 * np.pi / 180):
        #    fwd.append(val)
        #elif (160 * np.pi / 180) <= angle <= (200 * np.pi / 180):
        #    left.append(val)

        if abs(angle - np.pi / 2) < angle_size:
            fwd.append(val)
        elif abs(angle - np.pi) < angle_size:
            left.append(val)
        elif abs(angle) < angle_size or abs(angle - 2 * np.pi) < angle_size:
            right.append(val)

    zones[0] = min(left)
    zones[1] = min(fwd)
    zones[2] = min(right)

    prev_state = current_state
    current_state = (getState(zones[0]), getState(zones[1]), 
            getState(zones[2]))


def debug_logging():
    rospy.loginfo("Current State is: %s", current_state)
    rospy.loginfo("Current Action is: %s", current_action)


def updateAction():
    global prev_action, current_action
    prev_action = current_action
    q_vals = learning_table[current_state]
    current_action = Action(q_vals.index(max(q_vals)))


def publishAction():
    global pub
    mvmt = Pose2D()
    if current_action.value == 0:
        mvmt.theta = TRN_SPD
    elif current_action.value == 1:
        mvmt.theta = -1 * TRN_SPD
    elif current_action.value == 2:
        mvmt.theta = 0
    mvmt.y = LIN_SPD
    pub.publish(mvmt)


if __name__ == "__main__":
    try:
        rospy.init_node("wall_learner", anonymous=True)
        learning_table = reset_learning_table(learning_table)
        getLaserScan()
    
    except rospy.ROSInterruptionException:
        pass

