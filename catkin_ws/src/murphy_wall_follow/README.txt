How to run:
----------------
1. Do all of the installation stuff from the original README.
2. Put this package, murphy_wall_follow, next to stingray_sim in catkin_ws/src
3. Go to ~/Stingray-Simulation/catkin_ws
4. Run catkin_make
5. Source the setups the same way as the originals, so
	source /opt/ros/melodic/setup.bash
	source ~/Stingray-Simulation/catkin_ws/devel/setup.bash
	source ~/Stingray-Simulation/stingray_setup.bash

6. Finally, launch with
	roslaunch murphy_wall_follow murphy_deliverable_1_wall_follow.launch
